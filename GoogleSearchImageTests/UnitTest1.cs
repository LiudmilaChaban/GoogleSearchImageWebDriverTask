using GoogleSearchImageTests.PageObject;
using OpenQA.Selenium;
using static System.Net.Mime.MediaTypeNames;

namespace GoogleSearchImageTests
{
    public class Tests
    
    {
        public IWebDriver _driver;
       
        private const string imgurl = "https://ukrnationalism.com/media/k2/items/cache/048e752c64cf593538ebd02b789f68f2_L.jpg";


        [SetUp]
        public void Setup()
        {
            _driver = new OpenQA.Selenium.Chrome.ChromeDriver();
            _driver.Navigate().GoToUrl("https://www.google.com.ua/");
            _driver.Manage().Window.Maximize();

        }

        [Test]
        public void Test1()
        {
            var mainpage = new MainPageObject(_driver);
            mainpage
                .GoToImages()
                .SearchImage(imgurl)
                .SearchResult();                       
        }

        [TearDown]

        public void TearDown()
        {
           _driver.Quit();
        }
    }
}