﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleSearchImageTests.PageObject
{
    internal class MainPageObject
    {
        private IWebDriver _driver;
        private readonly By _imagesButton = By.XPath("//a[contains(@href, \"https://www.google.com.ua/imghp?hl=ru&ogbl\")]");

        public MainPageObject(IWebDriver driver)
        {
            _driver = driver;
        }

        public SearchImagePageObject GoToImages()
        {
           _driver.FindElement(_imagesButton).Click();
            return new SearchImagePageObject(_driver);
        }
    }
}
