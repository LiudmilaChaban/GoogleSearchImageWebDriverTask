﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleSearchImageTests.PageObject
{
    internal class SearchImagePageObject
    {
        private IWebDriver _driver;

        private readonly By _cameraImage = By.XPath("//img[contains(@alt, \"Поиск с помощью камеры\")]");
        private readonly By _imageUrlInputField = By.XPath("//input[contains(@jsname, \"W7hAGe\")]");
        private readonly By _searchButton = By.XPath("//div[contains(@jsname, \"ZtOxCb\")]");

        
        public SearchImagePageObject(IWebDriver driver)
        {
            _driver = driver;
        }
        
        public SearchResultPageObject SearchImage(string imgurl)
        {
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(40);

            _driver.FindElement(_cameraImage).Click();
            _driver.FindElement(_cameraImage).Click();
            _driver.FindElement(_imageUrlInputField).SendKeys(imgurl);
            _driver.FindElement(_searchButton).Click();
            return new SearchResultPageObject(_driver);
        }

    }
}
