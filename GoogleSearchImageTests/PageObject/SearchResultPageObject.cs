﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleSearchImageTests.PageObject
{
    internal class SearchResultPageObject
    {
        private IWebDriver _driver;

        private readonly By _descriptionResult = By.XPath("//div[contains(@class, \"UAiK1e\")]");

        public SearchResultPageObject(IWebDriver driver)
        {
            _driver = driver;
        }

        public void SearchResult()
        {
            var actualresult = _driver.FindElement(_descriptionResult).Text;

            Assert.That(actualresult, Is.Not.Null);

        }
    }
}
